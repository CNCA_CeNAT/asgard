 
FROM continuumio/miniconda3:4.9.2

RUN conda install python=3.6 -y

# Identify the maintainer of an image
LABEL maintainer="sr.alexe@gmail.com"

COPY scripts scripts

RUN rm -rf /var/lib/apt/lists/* 

RUN ./scripts/pip-dependencies.sh

RUN ./scripts/conda-dependencies.sh
RUN ./scripts/debian-dependencies.sh

COPY . /asgard
WORKDIR /asgard

