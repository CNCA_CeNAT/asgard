<!-- @format -->

<p align="center"><img width=60% src="./logo.png"></p>

# ASGARD+

## 1. Introduction:

<br>

ASGARD+ (Accelerated Sequential Genome-analysis and Antibiotic Resistance Detection) is a command line platform for automatic identification of antibiotic resistance genes in bacteria genomes, providing an easy-to-use interface to process big batches of sequence files from whole genome sequencing, with minimal configuration. It also provides a CPU optimization algorithm that reduces the processing time.

## 2. Required dependencies:

<br>

-   Note ASGARD+ has been tested with a Debian or Ubuntu based OS, however it could be adapted to run in other Linux distributions.

| LIBRARY         | VERSION |
| --------------- | ------- |
| Pygments        | 2.10.0  |
| ariba           | 2.14.4  |
| bcftools        | 1.9     |
| bowtie2         | 2.3.5.1 |
| bwa             | 0.7.17  |
| cd-hit          | 4.8.1   |
| colorama        | 0.4.4   |
| executing       | 0.8.1   |
| icecream        | 2.1.1   |
| jsonschema      | 3.2.0   |
| mummer          | 3.23    |
| munch           | 2.5.0   |
| numpy           | 1.19    |
| pydocstyle      | 5.1.1   |
| pyrsistent      | 0.17.3  |
| raxml           | 8.2.12  |
| requests        | 2.24.0  |
| samtools        | 1.9     |
| six             | 1.15.0  |
| snowballstemmer | 2.1.0   |
| snp-sites       | 2.5.1   |
| spades          | 3.13.1  |

-

## 3. Instalation:

<br>

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/CNCA_CeNAT/asgard.git
    ```

2. Navigate to the scripts directory inside the asgard repository:

    ```bash
    cd asgard/scripts
    ```

3. Run the three scripts to install the dependencies:

    ```bash
    conda-dependencies.sh
    debian-dependencies.sh
    pip-dependencies.sh
    ```

## 4. Usage:

<br>

ASGARD+ can be used with a single configuration file or you can specify a directory and it will execute all configuration files found inside it. This configuration files where created to excecute two specific pipelines; however you can create your own pipelines and execute them in the same way.
<br>

-   To run a single configuration file it must be called by executing the following command.

```bash
python asgard-plus.py single --config_file=<config file>
```

-   To run a multiple configuration files you need to specify the directory where the configuration files will be located.

```bash
python asgard-plus.py multiple --config_dir=<directory path>
```

-   ## Optional Arguments for single mode:

    | ARGUMENT          | DESCRIPTION                                     |
    | ----------------- | ----------------------------------------------- |
    | --checkpoint_file | Path to the checkpoint file to resume execution |
    | --output_dir      | (WIP) Path for the output directories           |

-   ## Optional Arguments for multiple mode:

    | ARGUMENT         | DESCRIPTION                                                                                    |
    | ---------------- | ---------------------------------------------------------------------------------------------- |
    | --checkpoint_dir | Path to the directory where the checkpoint files are located. (Name must match the ajson file) |

-   ## Optional Arguments:

    | ARGUMENT      | DESCRIPTION                                                                                |
    | ------------- | ------------------------------------------------------------------------------------------ |
    | -p, --preview | (WIP) Creates a checkpoint file and a execution summary without actually running the tool. |
    | -h,--help     | Shows information about the program and its arguments                                      |

    <br>

-   ## Considerations:

        If the configuration file is set to check for paired inputs all fastq files MUST come in pairs and all should be place in the same directory set as the input.

    <br>

---

## 5. Examples:

<br>

The first pipeline is designed for the identification of antimicrobial resistance genes with the ARIBA, it will process a multiple fastq files and will output the results in the directory specified in the configuration file.

```bash

python asgard-plus.py single --config_file=./config/asgard.ajson

```

## 6. Limitations:

<br>

-   For this version there is no way to use local databases.
-   The software does not check that all the dependencies are met.

<br>

# AJSON specification

ASGARD+ json files are an extension of the JavaScript Object Notation that provides references to internal and external properties of the objects. Certain elements must be present in the configuration file for the program to work.

<br>

## 1. Syntax

Files contained in the config_dir directory with the .ajson extension are treated as configuration files for the execution of ASGARD+

The configuration file is read from top to bottom and any reference values are resolved in the same manner.

-   Internal Objects
    -   Internal references are defined using double braces. The referenced property must be assigned before it is referenced. In this example the value of the color key inside the motorcycle object would be lightblue after the evaluation.

```json
{
    "motorcycle": {
        "variant": "light",

        "color": "{{variant}}blue",

        "year": 2010
    }
}
```

-   External Objects
    -   References to external objects are defined using double braces and using dot to navigate the object depth, all external references must be made from the top object and are case sensitive, in this example the color of the helmet will match the color of the motorcycle.

```json

{

    "motorcycle":{
        "color": "blue",
        "year": 2010
    },

    "helmet":{
        "color":"{{motorcycle.color}}"
    }
```

It is possible to create composite values from multiple references and strings.

The definition of the name/value pair must be defined before it is referenced so that it can be resolved properly.

## 2. Reserved words

<br>

-   Reserved words are used to define the values of the configuration file.

    ### constant:

        Contains non changing configuration parameters that can be referenced by other objects. Properties inside the "constant" object must not contain external references.

    <br>

    | Reserved Words      | Use Description                                                                                                                                                                                                     |
    | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | name                | Name of the script that will be executed, this name is used to generate the output directory.                                                                                                                       |
    | input_directory     | Directory with the fastq gz files forward and reverse. Each fastq file must have its pair in the same directory. Each pair is composed of a name and a suffix specified in the forward and reverse properties.      |
    | output_directory    | Directory where the output of each configuration file will be created. Each execution creates a new directory with an unique name at start of its execution, resulting files are then created inside this directory |
    | input_extension     | All files in the input directory ending with the input extension are listed and used for the execution of the commands.                                                                                             |
    | reference_accession | Accession number of the genomes to be downloaded and analysed. This file is downloaded with the fasta extension using NCBI efetch utility                                                                           |
    | accessory_accession | Accession number of the genome to be appended to the reference_accession fasta file.                                                                                                                                |
    | entrez_database     | Database from where the fasta file will be searched and downloaded                                                                                                                                                  |
    | workers             | Specifies the number of parallel jobs created of each command, each time a task finishes a new job is spawned with the next iteration.                                                                              |
    | forward             | Suffix of the forward files in the input directory.                                                                                                                                                                 |
    | reverse             | Suffix of the matching pair of the input fastq files.                                                                                                                                                               |
    | iterator            | Expandable bash expression that represents a list of files to iterate with the workflow. This expression can be a composite value. Other wildcards can be used for the filename expansion.                          |
    | dynamic             | This object contains information that is variable at run time, this enables it to iterate through the files present in the input directory.                                                                         |
    | prefix_regex        | Regular expressions that define the pattern of the valid filename without extension nor suffix.                                                                                                                     |
    | placeholder         | Symbol used as a placeholder for the fastq file names before its evaluation at runtime.                                                                                                                             |

    <br>

    ### execute:

        Each command can be executed in different modes depending on the number of iterations required. Each key and value pair describe the execution mode of each of the commands within the configuration file. The objects that describe the tasks of each command must have the same name as the key in the execute object. All commands with its respective task must be written after the execute object.

    <br>

    | Object             | Use Description mode                                                                                                                                                                                                                                                            |
    | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | single             | The object will be evaluated and will be executed one single time. Dynamic values should not be used in this command since these will not be evaluated.                                                                                                                         |
    | iterate-parallel   | The object will be executed in a new process created by the subprocess library, the number of parallel processes is determined by the workers constant. Dynamic placeholders will be evaluated when the new process is spawned. Filenames will be replaced in no logical order. |
    | iterate-sequential | The command object will be iterative but only one process is run at a time. Dynamic placeholders will be evaluated the same way as in iterate-parallel.                                                                                                                         |
    | false              | The task is disabled and will be ignored.                                                                                                                                                                                                                                       |
    |                    |                                                                                                                                                                                                                                                                                 |

## 3. Command Types

    Objects declared at the root level are checked for the <command> property, if this property is defined the program will queue its execution in the same order it was read. There are two types of commands that can be executed in the configuration file. The first ones are commands that are implemented in the ASGARD+ program and the second ones are commands that are implemented in external programs.

### Simple Commands

| Command         | Description                                                                                                                                                                                                                                                                                                                                                  |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| create_file     | Creates an empty text file in the specified in the _file_ parameter. Absolute path to the file is recommended.<br/> <b>Required parameters:</b><br/><tb/> file: Symbolic link to the new file to be created.                                                                                                                                                 |
| check_directory | Verifies that the directory exists, if not it creates one with the specified name, recursive creation of directories is enabled.<br/> <b>Required parameters: </b><br/>directory: Absolute path to be checked or created.                                                                                                                                    |
| entrez_download | This command downloads the fasta files using its accession number in the NCBI database. HTTPS GET request is used for the download.<br/> <b>Required parameters:</b><br/> url: https URL to the fasta file in the NCBI database. Use the constant accession variable. <br/>file: Symbolic link to the new file to be created.                                |
| merge           | The merge command enables the program to concatenate two or more text files into a new file. A new line is added between each file listed. <br/> <b>Required parameters:</b><br/> files: JSON list of the absolute or relative paths to the files to be merged.<br/> output_file: Path to the file to be created. If the file exists it will be overwritten. |
| replace         | Replaces all occurrences of a text value with a new string. <br/> <b>Required parameters:</b><br/> file: Path to the file where the text fragment will be replaced.<br/> old_data: Text to be replaced.<br/> new_data: The new text that will replace the old text fragment.                                                                                 |
|                 |

### Complex commands

<br>

    This are specified using a json array, dynamically generated items are evaluated and then executed sequentially. These commands are run using the subprocess library of python. If POSIX is being used, the path to the program must be the first parameter of the list.

    It is possible to add extra parameters to these complex commands, and will be passed to program. If the expansion of bash parameters is necessary, you can set the "shell" property to True if should be executed by the shell interpreter. These types of complex commands can be used to iterate over multiple files with similar names or traverse complex routings. You can run the same command multiple times with different files using the placeholder variable, this will be evaluated at runtime.  In order to enable file iteration, it is necessary to select the "iterate-parallel" or "iterate-sequential" execution modes.

#### Example:

In this case the program samtools must be accessible from the directory where ASGARD is being run, this can be achieved by setting the environmental variables or specifying the full path to the executable.

The values in the command list can be composite, constant, or strings.

```json

"sam_view": {
    "extension": ".bam",
    "file": "{{dynamic.output_file}}{{extension}}",
    "output_pipeline": "{{file}}",
    "command": ["samtools","view","-bS","-q","15","{{bwa_mem.file}}"]

},
```

### Default Configuration files.

    Two different configuration files are provided with the software one corresponding to ASGARD and the other one for SAGA. These configuration files implement the following pipeline.

<p align="center"><img width=60% src="./default_pipelines.jpg"></p>

## Authors

-   **Alex Saenz Rojas** - _Development and testing_ - [sr.alexe](https://gitlab.com/sr.alexe/)
-   **Maripaz Montero Vargas** - _Development and supervision_ - [mamontero](https://gitlab.com/mamontero/)
<!-- @format -->
