# ASGARD - Accelerated Sequential Genome-analysis and Antibiotic Resistance Detection
# Copyright(C) 2021 Alex Saenz Rojas and Maripaz Montero

# This program is free software: you can redistribute it and / or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation  either version 3 of the License,
# or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY  without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see < https://www.gnu.org/licenses/>.


import asgard.arguments_handler as args_handler
from asgard.constants import system
import asgard.configuration as config
import asgard.checkpoint as checkpoint
import asgard.scheduler as scheduler
import asgard.utils as utils

import logging


config_files, checkpoint_files = args_handler.parse_arguments(system.NAME,
                                                              system.DESCRIPTION,
                                                              system.EPILOG)

for config_file in config_files:

    new_config = config.initialize(config_file)

    log_file = utils.to_str(
        new_config.constant.output_directory) + "asgard.log"

    logging.basicConfig(
        filename=log_file, filemode='w',
        format='%(levelname)s: %(message)s %(asctime)s',
        datefmt='%m/%d/%Y %I:%M:%S %p')

    logger = logging.getLogger(new_config.constant.name)
    logger.setLevel(logging.DEBUG)
    logger.debug("Starting asgard")

    checkpoint.initialize(new_config.constant.output_directory,
                          new_config["execute"],
                          config_file.stem,
                          checkpoint_files)

    scheduler.run_config(new_config)

# pprint.pprint(conf)
