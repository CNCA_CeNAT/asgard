import argparse
from . import utils
from .constants import help
from .exceptions import NoConfigFilesError


def parse_arguments(name, description, epilog):

    parser = create_argument_parsers(name, description, epilog)
    args = parser.parse_args()
    return _validate_arguments(args)


def create_argument_parsers(name, description, epilog):

    parser = argparse.ArgumentParser(prog=name,
                                     description=description,
                                     epilog=epilog)
    parser.add_argument("--preview", action="store_true",
                        help=help.PREVIEW)

    # Choose between one file or multiple ajson files in a directory.
    config_subparsers = parser.add_subparsers(help="Select operating mode",
                                              dest="mode")
    parser_single_file = config_subparsers.add_parser(
        'single',
        help=help.MODE_SINGLE)
    parser_single_file.add_argument('--config_file',
                                    type=str,
                                    help=help.CONFIG_FILE,
                                    required=True,
                                    metavar="config_file")
    parser_single_file.add_argument('--checkpoint_file',
                                    type=str,
                                    help=help.CSV_FILE,
                                    metavar="checkpoint_file")

    parser_single_file.add_argument('--output_dir',
                                    type=str,
                                    help=help.CSV_FILE,
                                    metavar="output_dir")

    parser_multiple_file = config_subparsers.add_parser(
        'multiple',
        help=help.MODE_MULTIPLE)
    parser_multiple_file.add_argument('--config_dir',
                                      type=str,
                                      help=help.CONFIG_DIRECTORY,
                                      required=True,
                                      metavar="config_dir")
    parser_multiple_file.add_argument('--checkpoint_dir',
                                      type=str,
                                      help=help.CONFIG_DIRECTORY,
                                      metavar="checkpoint_dir")

    return parser


def _validate_arguments(args):

    args_dict = vars(args)
    config_files = []
    checkpoint_files = []

    if (args_dict.get("mode") == "multiple"):

        if(args_dict.get("config_dir")):
            config_files = utils.get_files(
                args_dict.get("config_dir"), "*.ajson")
        if(args_dict.get("checkpoint_dir")):
            checkpoint_files = utils.get_files(
                args_dict.get("checkpoint_dir"), "*.cjson")

        if(not config_files):
            raise NoConfigFilesError(
                str(args_dict["config_dir"].absolute()))

    elif (args_dict.get("mode") == "single"):

        if(args_dict.get("config_file")):
            config_files = [utils.is_file(
                args_dict.get("config_file"))]
        if(args_dict.get("checkpoint_file")):
            checkpoint_files = [utils.is_file(
                args_dict.get("checkpoint_file"))]

        if(not config_files):
            raise NoConfigFilesError(
                str(args_dict["config_file"].absolute()))

    return config_files, checkpoint_files
