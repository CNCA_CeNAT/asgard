from .exceptions import CommandNotFoundInConfig, NoInputFilesError,\
    UnMatchPairsError
from functools import reduce
import operator
from jsonschema import Draft7Validator
import json as json
import munch
import re
from . import utils
import numpy as np


def initialize(ajson_file):
    config = _load_json(ajson_file)
    _validate_schema(config)
    _validate_files_dirs(config)
    _resolve_json(config, config)
    _check_execute(config)
    _check_input(config)
    _check_pairs(config)

    return config


def _load_json(ajson_file):
    json_config = json.load(open(ajson_file, "rt"))
    return munch.munchify(json_config)


def _validate_schema(config):
    json_schema = json.load(open(f"config/ajson-schema.json", "rt"))
    json_validator = Draft7Validator(json_schema)
    json_validator.validate(config)


def _validate_files_dirs(config):
    config.constant.input_directory = utils.get_dir(
        config.constant.input_directory)

    config.constant.output_directory = utils.get_dir(
        config.constant.output_directory, create_if_missing=True,
        append_datetime=True, suffix=config.constant.name)


def _resolve_json(rootObject, jObject, parent=None):
    for jValue in jObject:
        if isinstance(jObject[jValue], munch.Munch):
            _resolve_json(rootObject, jObject[jValue], jObject)
        elif isinstance(jObject[jValue], str):
            jObject[jValue] = _resolve_reference(
                rootObject, jObject[jValue], jObject)
        elif isinstance(jObject[jValue], int):
            pass
        elif isinstance(jObject[jValue], list):
            for index, jItem in enumerate(jObject[jValue]):
                if isinstance(jItem, str):
                    jObject[jValue][index] = _resolve_reference(
                        rootObject, jItem, jObject)


def _resolve_reference(rootObject, jString, parent):
    regex_pattern = '{{((?:.*?))}}'
    for match in re.findall(regex_pattern, jString):
        if "." in match:
            reference_match = reduce(
                operator.getitem, match.split('.'), rootObject)
        else:
            reference_match = parent[match]

        jString = jString.replace(
            f"{{{{{match}}}}}", utils.to_str(reference_match))

    return jString


def _check_execute(config):
    execute_keys = np.array(list(config["execute"].keys()))
    config_keys = np.array(list(config.keys()))
    orphans_commands = np.isin(execute_keys, config_keys, invert=True)
    if execute_keys[orphans_commands].size > 0:
        raise CommandNotFoundInConfig(
            ", ".join(config_keys[orphans_commands]))


def _check_input(config):
    input_files = config.constant.input_directory.glob(
        config.constant.input_file_pattern)
    if len(list(input_files)) <= 0:
        NoInputFilesError(str(config.constant.input_directory))


def _check_pairs(config):
    if (config.constant.paired_input):
        forward_files = get_prefixes(config, config.constant.forward)
        reverse_files = get_prefixes(config, config.constant.reverse)
        unmatched_pairs = forward_files.symmetric_difference(reverse_files)

        if len(unmatched_pairs) > 0:
            raise UnMatchPairsError(str(unmatched_pairs))


def get_prefixes(config, suffix):

    extension = config.constant.input_extension
    files = config.constant.input_directory.glob(
        "*"+suffix+extension)
    files_ids = set(file.name.replace(suffix+extension, "")
                    for file in files)

    return files_ids


def resolve_iteration(jObject, iteration_value, placeholder):
    for jValue in jObject:
        if isinstance(jObject[jValue], list):
            for index, jItem in enumerate(jObject[jValue]):
                if isinstance(jItem, str):
                    jObject[jValue][index] = _replace_iteration(
                        jItem, iteration_value, placeholder)
        elif isinstance(jObject[jValue], str):
            jObject[jValue] = _replace_iteration(
                jObject[jValue], iteration_value, placeholder)

    return jObject


def _replace_iteration(jString, iteration_value,  placeholder):
    findings = re.findall(placeholder, jString)
    for match in findings:
        jString = jString.replace(
            f"{match}", utils.to_str(iteration_value))

    return jString
