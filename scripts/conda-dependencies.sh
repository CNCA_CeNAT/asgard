#!/bin/bash

conda install -c bioconda \
ariba=2.14.4 \
bcftools=1.9 \
bowtie2=2.3.5.1 \
bwa=0.7.17 \
cd-hit=4.8.1 \
mummer=3.23 \
raxml=8.2.12 \
samtools=1.9 \
snp-sites=2.5.1 \
spades=3.13.1 -y