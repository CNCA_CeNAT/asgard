#!/bin/bash

apt-get -qq update --allow-releaseinfo-change -- && apt-get install --no-install-recommends -y \
  autoconf \
  automake \
  build-essential \
  curl \
  gcc \
  git \
  libbz2-dev \
  libcurl4-gnutls-dev \
  liblzma-dev \
  libssl-dev \
  libtbb-dev \
  locales-all \
  make \
  perl \
  unzip \
  wget \
  zlib1g-dev 