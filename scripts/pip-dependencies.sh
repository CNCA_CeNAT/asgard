#!/bin/bash

## Get the root path to this bash script.
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

## Install the dependencies from the requirements.txt file located in the same directory as this script.
pip3 install -r $SCRIPT_DIR/requirements.txt
