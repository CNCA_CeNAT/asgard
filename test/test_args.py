import unittest
from icecream import ic
from .context import asgard


class test_args(unittest.TestCase):

    def setUp(self):
        self.parser = asgard.arguments_handler.create_argument_parsers(
            "Name", "Description", "Epilog")

    def test_parser_help(self):
        parsed_arguments = self.parser.parse_args(["--help"])
        ic(parsed_arguments)


if __name__ == '__main__':
    unittest.main()
